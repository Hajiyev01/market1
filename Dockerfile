FROM openjdk:17
COPY build/libs/market-0.0.1-SNAPSHOT.jar /market-app/
CMD ["java", "-jar","/market-app/market-0.0.1-SNAPSHOT.jar"]
