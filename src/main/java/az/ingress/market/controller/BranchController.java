package az.ingress.market.controller;



import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.dto.RequestBranchDto;
import az.ingress.market.entity.Branch;
import az.ingress.market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;

    @PostMapping
    public long saveBranch(@RequestBody RequestBranchDto request) {
        return branchService.saveBranch(request);
    }

    @GetMapping
    public  BranchQueryDto getBranch() {
        return branchService.getBranch();
    }


}