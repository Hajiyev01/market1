package az.ingress.market.controller;


import az.ingress.market.dto.MarketRequestDto;
import az.ingress.market.dto.MarketResponseDto;
import az.ingress.market.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public Long createMarket(@RequestBody @Valid MarketRequestDto request) {
        return marketService.createMarket(request);
    }

    @GetMapping
    public List<MarketResponseDto> getMarket() {
        return marketService.getMarket();
    }
}
