package az.ingress.market.controller;

import az.ingress.market.dto.RequestAddressDto;
import az.ingress.market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping
    public long saveAddress(@RequestBody RequestAddressDto request) {
        return addressService.saveAddress(request);
    }
}
