package az.ingress.market.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String branchName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "m_id", referencedColumnName = "id")
    @JsonIgnore
    Market market;

    @OneToOne(cascade =CascadeType.ALL )
    @JoinColumn(name = "a_id", referencedColumnName = "id")
    @JsonIgnore
    Address address;
}

