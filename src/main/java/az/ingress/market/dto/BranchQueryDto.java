package az.ingress.market.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BranchQueryDto {
    private Long branchId;
    private  String branchName;
    private  String addressName;
    private  String marketName;


}
