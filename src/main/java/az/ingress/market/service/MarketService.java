package az.ingress.market.service;

import az.ingress.market.dto.MarketRequestDto;
import az.ingress.market.dto.MarketResponseDto;
import az.ingress.market.entity.Market;
import org.springframework.stereotype.Service;

import java.util.List;

public interface MarketService {
    Long createMarket(MarketRequestDto request);

    List<MarketResponseDto> getMarket();

    Market getMarketWithId(Long id);
}
