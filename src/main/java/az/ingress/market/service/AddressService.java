package az.ingress.market.service;


import az.ingress.market.dto.RequestAddressDto;
import az.ingress.market.entity.Address;
import org.springframework.stereotype.Service;

@Service
public interface AddressService {
    long saveAddress(RequestAddressDto request);
    Address findById(long id);
}
