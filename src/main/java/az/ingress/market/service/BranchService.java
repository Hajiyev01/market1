package az.ingress.market.service;

import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.dto.RequestBranchDto;
import az.ingress.market.entity.Branch;

public interface BranchService {
    long saveBranch(RequestBranchDto request);

    BranchQueryDto getBranch();
}
