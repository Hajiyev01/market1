package az.ingress.market.service.impl;

import az.ingress.market.config.AppConfiguration;
import az.ingress.market.dto.MarketRequestDto;
import az.ingress.market.dto.MarketResponseDto;
import az.ingress.market.entity.Market;
import az.ingress.market.repository.MarketRepository;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final AppConfiguration appConfiguration;

    @Override
    public Long createMarket(MarketRequestDto request) {
        Market market = appConfiguration.getMapper().map(request, Market.class);
        return marketRepository.save(market).getId();
    }

    @Override
    public List<MarketResponseDto> getMarket() {
        List<Market> marketList = marketRepository.findAll();
        List<MarketResponseDto> marketResponseDtoList = new ArrayList<>();
        marketList.forEach(entity -> {
            MarketResponseDto marketResponseDto =
                    appConfiguration.getMapper().map(entity, MarketResponseDto.class);
            marketResponseDtoList.add(marketResponseDto);
        });
        return marketResponseDtoList;
    }


    @Override
    public Market getMarketWithId(Long id) {
        return marketRepository.findById(id).orElseThrow(()->new RuntimeException("Market not found with id" +id));
    }
}
