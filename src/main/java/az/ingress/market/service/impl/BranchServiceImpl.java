package az.ingress.market.service.impl;


import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.dto.RequestBranchDto;
import az.ingress.market.entity.Address;
import az.ingress.market.entity.Branch;
import az.ingress.market.entity.Market;
import az.ingress.market.repository.BranchRepository;
import az.ingress.market.service.AddressService;
import az.ingress.market.service.BranchService;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final
    MarketService marketService;
    private final AddressService addressService;
    private final ModelMapper modelMapper;
    @Override
    public long saveBranch(RequestBranchDto request) {
        Branch branch = modelMapper.map(request, Branch.class);
        
        return branchRepository.save(branch).getId();
    }

    @Override
    public BranchQueryDto getBranch() {

        BranchQueryDto branchQueryDto = branchRepository.getBranchWithCustomDtoQuery();
        log.info("Getting branch from DB : {}", branchQueryDto);
        return branchQueryDto;
    }}