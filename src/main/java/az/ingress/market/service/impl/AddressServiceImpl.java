package az.ingress.market.service.impl;

import az.ingress.market.dto.RequestAddressDto;
import az.ingress.market.entity.Address;
import az.ingress.market.repository.AddressRepository;
import az.ingress.market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;
    @Override
    public long saveAddress(RequestAddressDto request) {
        Address address = modelMapper.map(request, Address.class);
        return addressRepository.save(address).getId();
    }



    @Override
    public Address findById(long id) {
        Optional<Address> addressOptional = addressRepository.findById(id);
        addressOptional.orElseThrow(() -> new RuntimeException());
        return addressOptional.get();
    }
}
